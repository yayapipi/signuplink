const {
  createApp,
  ref,
  onMounted,
  inject,
  watch
} = Vue

createApp({
  setup() {
    const platform = ref('Quest2');
    const placeholder = ref('Quest 2 user: enter your Oculus ID email');
    const email = ref('');

    // onMounted(() => {
    //   // 顯示 modal
    //   var myModal = new bootstrap.Modal(document.getElementById('exampleModal'), {
    //     keyboard: false
    //   });
    //   myModal.show()
    // })

    // 更改 placeholder 文字
    watch(platform, () => {
      if(platform.value == 'PCVR') {
        placeholder.value = 'Enter your email to receive your Steam key'
      } else {
        placeholder.value = 'Quest 2 user: enter your Oculus ID email'
      }
    })

    function getKey() {
      // 沒填寫 email 打回去
      if(email.value == '') {
        alert('請填寫 email');
        return false
      }

      // Email 正規驗證
      emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;

      if(email.value.search(emailRule) != -1) {
        // email 驗證成功
        // 打 DC 和 tg 
        Promise.all([DCgetKey(), telegramGetKey()])
        .then((res) => {
          const item1 = res[0].status;
          const item2 = res[1].status;
          // 如果兩方都成功才會顯示 modal
          if(item1 && item2) {
            // 顯示 modal
            var myModal = new bootstrap.Modal(document.getElementById('exampleModal'), {
              keyboard: false
            });
            myModal.show()
            // 恢復欄位
            platform.value = 'Quest2'
            email.value = ''
          } else {
            alert('Error, 錯誤');
          }
        })
        .catch(() => {
          console.log(error);
          alert('Error, 錯誤');
        })
      } else {
        // email 驗證失敗
        alert('Please enter a valid email address');
      }
    }

    function telegramGetKey() {
      return new Promise((resolve, reject) => {
        // telegram url
        const tgUrl = 'https://api.telegram.org/bot5638724293:AAFVjDH49UJLt-mN2NIxd19Ce73sf_ufve8/sendMessage';

        // 組 text 參數值
        const newString = `➠ ${platform.value}   ✉ ${email.value}`

        const data = {
          text: newString,
          chat_id: -1001573858830
        };

        // 打 tg API
        axios.post(tgUrl, data)
        .then((res) => {
          resolve({
            status: true,
            message: 'telegram success'
          });
        })
        .catch((error) => {
          reject({
            status: false,
            message: 'telegram fail'
          });
        })
      })
    }

    function DCgetKey() {
      return new Promise((resolve, reject) => {
        // dc url
        const dcUrl = 'https://discord.com/api/webhooks/1055326745437405246/LxF8hMWzeph46GQeYtGNCZz01BEN6RgInwSNJTBJ6JrLoi5Q0rRwVYtt1XTzaT7EkRYY';

        // 組 content 參數值
        const newString = `➠ ${platform.value}   ✉ ${email.value}`

        const data = {
          content: newString,
          chat_id: -1001573858830
        };

        // 打 DC API
        axios.post(dcUrl, data)
          .then((res) => {
            resolve({
              status: true,
              message: 'DC success'
            });
          })
          .catch((error) => {
            reject({
              status: false,
              message: 'DC fail'
            });
          })
      })
    }
    
    return {
      platform,
      placeholder,
      email,
      getKey,
      DCgetKey,
      telegramGetKey
    }
  }
}).mount('#app')
